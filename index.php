<?php
/**
 * The main template file
 *
 * @package WordPress
 * @subpackage AthenaVirtual
 * @since AthenaVirtual 1.0
 */
 ?>
 <!DOCTYPE html>

 <html <?php language_attributes(); ?> class="no-js">
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="shortcut icon" href="/favicon.ico">
    <link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">
    <title>Athyna - The new way to hire a Virtual Assistant</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135926874-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-135926874-1');
    </script>
	  <!-- End of Global site tag (gtag.js) - Google Analytics -->
    <!-- End of YotPo Widget script -->
    <script type="text/javascript">
    (function e(){var e=document.createElement("script");e.type="text/javascript",e.async=true,e.src="//staticw2.yotpo.com/M2oZvGiZq5VLR62XhQ132qZANzLS7mbPKgS20QIJ/widget.js";var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)})();
    </script>
    <!-- End of YotPo Widget script -->
    <script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js">
    </script>
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v6.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your customer chat code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="100723034826203"
  theme_color="#0084ff"
  logged_in_greeting="Welcome to Athyna! What can we help you with today?"
  logged_out_greeting="Welcome to Athyna! What can we help you with today?">
      </div>
    <!-- End Facebook chat plugin -->
    <!-- Start of Sumo script -->
    <script async>(function(s,u,m,o,j,v){j=u.createElement(m);v=u.getElementsByTagName(m)[0];j.async=1;j.src=o;j.dataset.sumoSiteId='c6d166f86bfddbda7d5b8c4b617a4562f3213067814b8261e399894c6ab4dc43';v.parentNode.insertBefore(j,v)})(window,document,'script','//load.sumo.com/');</script>
    <!-- End of Sumo script -->
    <div id="main">
      <?php wp_footer(); ?>
    </div>
  </body>
</html>