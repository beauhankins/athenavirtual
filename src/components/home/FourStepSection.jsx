import React from 'react';
import Section from '../shared/Section';

export default () => {
  const renderStep = ({ icon, title, content }) => (
    <div className="step">
      <div
        className="stepIcon"
        style={{ backgroundImage: `url(${icon})` }}
      />
      <div className="flex-auto">
        <h3>{title}</h3>
        <p>{content}</p>
      </div>
    </div>
  );

  const steps = [{
    icon: `${AssetsPath}/enquiry.png`,
    title: 'send an enquiry',
    content: (
      <span>
        Simply submit an enquiry and we’ll be ready to ask you the right questions to determine your needs. We’ll then send you a list of candidates from our team of vetted and educated workers. You will have the opportunity to interview them and choose your desired VA.
      </span>
    )
  }, {
    icon: `${AssetsPath}/package.png`,
    title: 'choose your package',
    content: (
      <span>
        Once you have made your decision, you can choose between our 3 packages:<br/>
        <ul>
          <li>Entry package for 20 hours per week.<br/></li>
          <li>Premium package for 40 hours per week. <br/></li>
          <li>Enterprise package for 80+ hours per week.<br/></li>
        </ul>
      </span>
    )
  }, {
    icon: `${AssetsPath}/support.png`,
    title: 'receive ongoing support',
    content: (
      <span>
        We will sign you up to our #HardWorkIsForLosers System that will provide you with the support you need to develop a strong and mutually beneficial working relationship with your chosen VA.
      </span>
    )
  }, {
    icon: `${AssetsPath}/calendar.png`,
    title: 'build it in 90 days',
    content: (
      <span>
        We believe it takes at least 3 months to develop a solid working relationship. We'll be with you every step of the way to help you get used to working with your VA, and achieve success. That’s how much we believe in our process and our team.
      </span>
    )
  }];

  return (
    <Section
      className="fourStepSection"
      color="#3B455C"
      backgroundColor="#D8E3E8">
      <h2>alright, so how does this work...</h2>

      <div className="grid">
        <div className="row">
          {renderStep(steps[0])}
          {renderStep(steps[1])}
        </div>

        <div className="row">
          {renderStep(steps[2])}
          {renderStep(steps[3])}
        </div>
      </div>

      <div className="cta">
        <a className="button inline large" href="/enquire">Enquire Now</a>
      </div>
    </Section>
  );
}