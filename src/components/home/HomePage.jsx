import React from 'react';
import { Link } from 'react-router-dom';

import HeroSection from '../shared/HeroSection';
import FourStepSection from './FourStepSection';
import VideoSection from './VideoSection';
import GuaranteeSection from '../shared/GuaranteeSection';
import TestimonialSection from './TestimonialSection';

export default () => (
  <div>
    <HeroSection
      backgroundImage={`${AssetsPath}/hero2.webp`}>
      <div>
        <h1>Welcome to Athyna,<br/>the new way to hire a virtual assistant.</h1>
        <Link to="/enquire" className="button inline" style={{ marginTop: '1em' }}>
          Book A Live Demo
        </Link>
      </div>
    </HeroSection>
    <FourStepSection />
    <GuaranteeSection
      backgroundColor="#F5F5F4"
    />
    <TestimonialSection />
  </div>
)