import React from 'react'
import Section from '../shared/Section';

export default () => (
  <Section
    className="videoSection"
    color="#3B455C"
    backgroundColor="#D8E3E8">
    <div className="videoContainer">
      <div style={{ position: 'relative', paddingTop: '56.25%' }}>
        <iframe
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%'
          }}
          src="https://www.youtube.com/embed/67o78uGW8Js"
          frameborder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        />
      </div>
    </div>
  </Section>
)