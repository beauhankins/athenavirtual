import React from 'react'
import Section from '../shared/Section';
import cx from 'classnames';

export default class TestimonialSection extends React.Component {
  constructor() {
    super(...arguments);

    this.state = {
      currentPage: 0
    };

    this.handleBreadcrumbClick = this.handleBreadcrumbClick.bind(this);
  }

  render() {
    const { currentPage } = this.state;
    return (  
      <Section
        className="testimonialSection"
        color="#3B455C"
        backgroundColor="#F5F5F4">
        <div className="testimonialsContainer">
          {this.renderTestimonial(this.testimonials()[currentPage])}
        </div>
        <div className="breadcrumbs">
          {this.testimonials().map((_, i) =>
            <div
              className={cx('breadcrumb', { active: i === currentPage })}
              onClick={this.handleBreadcrumbClick(i)}
            />
          )}
        </div>
      </Section>
    )
  }

  renderTestimonial({ title, body, image }) {
    return (
      <div className="testimonial">
        {/*<img
          className="testimonialImage"
          src={`${AssetsPath}/${image}`}
        />*/}
        <div>
          <h5>What our customers have to say</h5>
          <h2 className="bold">{title}</h2>
          <p>
            {body}
          </p>
        </div>
      </div>
    );
  }

  testimonials() {
    return [{
      title: 'Alan Romero | Romero Athletics',
      body: 'I had been thinking about testing out a virtual assistant for nearly 6 months since first hearing about the idea. My only hesitation quickly faded as I realised I needed to do something to get some time back in the business. I decided to take the plunge and trial out an assistant in Venezuela for 10 hours per week. I have no upgraded to 15 hours per week and I’m not sure how I ever managed the business without her assistance. If you are on the fence about a VA, just try it.'
    }, {
      title: 'Raph Freedman | The Mind Muscle Project',
      body: 'We are now 6 months with our first VA, and 1 month with our second. At first I didn’t think the VA would be worth the money, because there was no way I would find 10 hours of stuff for him or her to do, but I did it any just to see if there were a few things I could get help with. For the first month I struggled to find the time to pass any meaningful work. Fast forward 6 months, and now the VA is the bed-rock of our business. We have literally had the best 6 months of our 5 year existence, and hiring overseas help is the only thing we have changed. It has saved me 20-30 hours a week of ‘busy work’, and has freed up my time to grow my business, instead of just keep it above water.'
    }, {
      title: 'Adam Hackney | Bonus Money',
      body: 'Gaining extra teammates through Athyna has been a massive help to our business. As a small business owner, I’ve often been reluctant to hand over tasks to others, fearful that the cost would exceed the benefit. With Bill’s help onboarding the new starts to the team this wasn’t an issue. It has meant my time can be utilised focusing on key areas of the business such as growth. We started with getting help for data entry but as we grew more confident in the ability and competency of the guys we now have our customer service, data, and some of our web content and design managed from the team through Athyna. We view all the guys we have hired through Athyna as equally cherished part of the team as the local crew.'
    }, {
      title: 'Alex Lee | McQueen Group',
      body: 'Athyna has become a great asset to our business. It has allowed McQueen to expand our team without the feeling of our work being delegated via an outsourcing group. Having a single person that you are able to contact directly if there is a time critical issue has been so helpful, especially with the daily challenges of COVID-19. Being able to teach and develop a person the methods of our business and the client experience we are trying to deliver has allowed us to have absolute confidence in the staff member supplied by Athyna. From the outset, Doc and Athyna have showed great care and thought towards how they provide you with a suitable candidate. They understand that is isn’t as simple as a one size fits all scenario, hence, providing you with multiple options and the ability to choose your candidate not only on their skill, but their personality to ensure they are the right fit for your business. Once we started our journey with Athyna, Doc made regular touch points during the on-boarding process, which provided a seamless integration of our businesses.'
    }];
  }

  handleBreadcrumbClick(page) {
    return () => this.setState({ currentPage: page });
  }
}