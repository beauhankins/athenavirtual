import React from 'react';
import cx from 'classnames';
import { Link } from 'react-router-dom';

export default ({ name, hours, price, features, backgroundColor, large }) => (
  <div className={cx('pricingPlan', { large })} style={{ backgroundColor }}>
    <h2 className="bold">{name}</h2>
    <p>{hours} hrs a week</p>
    <br/>

    <p className="bold">What you could acheive:</p>
    {features.map(feature =>
      <span>- {feature}<br/></span>
    )}
    <br/>
    {large && <span><br/><br/></span>}

    <p className="bold">How much will it cost:</p>
    ${price} per hour<br/>

    <Link
      to="/enquire"
      className="button inline outline"
      style={{ color: '#FFFFFF' }}>
      Enquire Now
    </Link>
  </div>
)