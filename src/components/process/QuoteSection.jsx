import React from 'react'
import Section from '../shared/Section';

export default () => (
  <Section
    className="quoteSection processQuoteSection"
    backgroundColor="#D8E3E8">
    <h2>we’ve engineered the #HardWorkIsForLosers system,<br/>a four step process to get your life back<br/>and give back life.</h2>
    <div
      className="processFourStepIcons"
      style={{ backgroundImage: `url(${AssetsPath}/fourstep.png)` }}
    />
  </Section>
)