import React from 'react'
import Section from '../shared/Section';
import PricingPlan from './PricingPlan';

export default () => (
  <Section
    className="pricingSection"
    backgroundColor="#FFFFFF">
    <h2 className="bold">what packages can I choose from?</h2>
    <div className="pricingPlans">
      <PricingPlan
        name="entry"
        hours={12}
        price="10.5"
        backgroundColor="#87939B"
        features={[
          'Book-keeping and invoicing.',
          'Social media marketing.',
          'Email communications.',
          'Copywriting.',
          'Website management.'
        ]}
      />

      <PricingPlan
        name="standard"
        hours={20}
        price="9.25"
        backgroundColor="#3B455C"
        large
        features={[
          'Full marketing automations.',
          'Podcast and video editing.',
          'Content creation.',
          'Community management.',
          'Travel and event co-ordination.'
        ]}
      />

      <PricingPlan
        name="premium"
        hours={40}
        price="6.75"
        backgroundColor="#8097A4"
        features={[
          'A full-time virtual assistant.',
          'Round-the-clock productivity.',
          'Cover all your business needs.',
          'Growth at the fraction of the cost.',
          'Less micro-management, more time in  your life.'
        ]}
      />
    </div>
  </Section>
)