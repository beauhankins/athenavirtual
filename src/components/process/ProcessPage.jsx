import React from 'react'

import HeroSection from '../shared/HeroSection';
import QuoteSection from './QuoteSection';
import ProcessSection from './ProcessSection';
import PricingSection from './PricingSection';
import YotpoSection from './YotpoSection';

export default () => (
  <div>
    <HeroSection
      backgroundImage={`${AssetsPath}/hero1.webp`}
      showEnquireCard
    />
    <QuoteSection />
    <ProcessSection />
    <PricingSection />
    <YotpoSection />
  </div>
)