import React from 'react';
import moment from 'moment';
import Safe from 'react-safe';
import { ArrowRight } from 'react-feather';
import { Link } from 'react-router-dom';
import { PulseLoader as Loader } from 'react-spinners';
import Section from '../shared/Section';
import HeroSection from '../shared/HeroSection';
import Api from '../../lib/api';

export default class BlogPage extends React.Component {
  constructor() {
    super(...arguments);

    this.state = {
      posts: null
    };
  }

  componentDidMount() {
    this.fetchPosts();
  }

  fetchPosts() {
    const api = new Api(AthenaVirtual.URL.api);
    api.getAllPosts().then(posts => {
      this.setState({ posts });
    });
  }

  render() {
    const { posts } = this.state;

    return (
      <div>
        <HeroSection style={{ backgroundPosition: '50% 100%' }} backgroundImage={`${AssetsPath}/heroblog.webp`} />
        <Section>
          <h2 className="align-center">Athyna Blog</h2>
          <br/>
          {!posts && <div className="flex justify-center"><Loader size={10} color="#3A455C"/></div>}
          {posts && posts.map(post =>
            <div key={post.id} className="blogListItemWrapper">
              <Link to={`blog/${post.slug}`} className="blogListItem">
                <h3>{post.title.rendered}</h3>
                <h5>{post.category && `${post.category.name} • `}{moment(post.date).format("MMMM Do YYYY")}</h5>
                <Safe.p>
                  {post.content.rendered}
                </Safe.p>
                <div className="arrow">
                  <ArrowRight/>
                </div>
              </Link>
            </div>
          )}
          {posts && posts.length < 1 &&
            <p className="align-center">There are no blog posts yet...</p>
          }
        </Section>
      </div>
    );
  }
}