import React from 'react';
import { Link } from 'react-router-dom';
import { SocialIcon } from 'react-social-icons';

export default () => {
  const iconProps = {
    fgColor: '#ffffff',
    bgColor: 'none',
    style: { width: 50, height: 50 },
    target: '_blank'
  };
  return (
    <footer>
      <div className="footer">
        <div className="footerInner">
          <div className="newsletter">
            <div className="logo">
              <Link to="/">
                <img
                  src={`${AthenaVirtual.URL.root}wp-content/themes/athenavirtual/dist/images/athenalogowhite.svg`}
                />
              </Link>
            </div>
            <form
              method="POST"
              action="https://athenavirtual.activehosted.com/proc.php">
              <input type="hidden" name="u" value="3" />
              <input type="hidden" name="f" value="3" />
              <input type="hidden" name="s" />
              <input type="hidden" name="c" value="0" />
              <input type="hidden" name="m" value="0" />
              <input type="hidden" name="act" value="sub" />
              <input type="hidden" name="v" value="2" />
              <h4>Sign up to hear our weekly podcast and more</h4>
              <input
                type="text"
                className="field simple"
                placeholder="email"
                name="email"
              />
              <div className="flex justify-end">
                <button type="submit" className="button inline small white" style={{ marginTop: '1em' }}>
                  Submit
                </button>
              </div>
            </form>
          </div>
          <div className="links">
            <Link to="/">
              Home
            </Link>
            <Link to="/blog">
              Blog
            </Link>
            <Link to="/podcast">
              Podcast
            </Link>
            <Link to="/about">
              About
            </Link>
            <Link to="/process">
              Want To Get 20 Hours Free VA Work?
            </Link>
            <Link to="/enquire">
              Enquire
            </Link>
          </div>
          <div className="social">
            <h4>Follow us</h4>
            <div className="socialIcons flex items-center">
              <SocialIcon url="https://www.facebook.com/athyna.co/" {...iconProps} />
              <SocialIcon url="https://www.instagram.com/athyna.co/" {...iconProps} />
              <SocialIcon url="https://www.youtube.com/channel/UC5CoVNUpK6QKOOkSWAtBgMw" {...iconProps} />
              <SocialIcon url="https://podcasts.apple.com/podcast/the-athyna-way/id1478061387" {...iconProps} />
              <SocialIcon url="https://medium.com/athyna-co" {...iconProps} />
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}