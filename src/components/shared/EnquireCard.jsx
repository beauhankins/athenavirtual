import React from 'react';
import { Link } from 'react-router-dom';

export default () => (
  <div className="enquireCard">
    <div className="logo">
      <img src={`${AthenaVirtual.URL.root}wp-content/themes/athenavirtual/dist/images/athenalogo.svg`} />
    </div>
    <h3>Virtual Assistant Packages</h3>
    <p>12 hours, 20 hours, 40 hours<br/><br/></p>
    <p>Submit an enquiry and we’ll call you back to talk through your needs.</p>
    <Link to="/enquire">
      <div className="button inline">
        Enquire Now
      </div>
    </Link>
  </div>
)