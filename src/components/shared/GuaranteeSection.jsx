import React from 'react'
import Section from '../shared/Section';

export default ({ backgroundColor }) => (
  <Section className="guaranteeSection" backgroundColor={backgroundColor}>
    <div className="guaranteeCard">
      <h5>Our #HardWorkIsForLosers System</h5>
      <h2 className="bold">4 x 90 day phases to build your business</h2>
      <p>What sets us apart is our #HardWorkIsForLosers System, which we’ve developed to ensure you and your VA have a successful relationship. Our goal is to get you and your assistant working autonomously and both loving the process!</p>
    </div>
  </Section>
);