import React from 'react'
import Section from '../shared/Section';
import EnquireCard from './EnquireCard';

export default class HeroSection extends React.Component {
  render() {
    const { backgroundImage, showEnquireCard, style, children } = this.props;

    return (
      <Section
        className="heroSection"
        backgroundColor="#ACBBC5"
        backgroundImage={backgroundImage}>
        <div className="darken"/>
        <div className="heroContent">
          {children}
        </div>
        {showEnquireCard && <EnquireCard/>}
      </Section>
    );
  }
}