import React from 'react'
import { X } from 'react-feather';

export default class NewsletterCard extends React.Component {
  constructor() {
    super(...arguments);

    const subscribeClosed = localStorage.getItem('subscribeClosed') === 'true';

    this.state = {
      subscribeClosed
    };

    this.handleCloseClick = this.handleCloseClick.bind(this);
  }

  handleCloseClick() {
    localStorage.setItem('subscribeClosed', true);
    this.setState({ subscribeClosed: true });
  }

  render() {
    const { subscribeClosed } = this.state;
    
    return (
      <div className="newsletterCard" style={subscribeClosed ? { display: 'none' } : {}}>
        <div onClick={this.handleCloseClick} className="close"><X size={16}/></div>
        <p>Receive weekly entrepreneurial tips and insights.</p>
        <form
          method="POST"
          action="https://athenavirtual.activehosted.com/proc.php">
          <input type="hidden" name="u" value="3" />
          <input type="hidden" name="f" value="3" />
          <input type="hidden" name="s" />
          <input type="hidden" name="c" value="0" />
          <input type="hidden" name="m" value="0" />
          <input type="hidden" name="act" value="sub" />
          <input type="hidden" name="v" value="2" />
          <input
            type="text"
            className="field simple"
            placeholder="email"
            name="email"
            style={{ color: '#3B455C' }}
          />
          <br/>
          <div className="flex justify-end">
            <button type="submit" className="button inline small">
              Submit
            </button>
          </div>
        </form>
      </div>
    );
  }
}