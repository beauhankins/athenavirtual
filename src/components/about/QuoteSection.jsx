import React from 'react'
import Section from '../shared/Section';

export default () => (
  <Section
    className="quoteSection"
    backgroundColor="#F5F5F4">
    <h2>the greek god prometheus created man from clay<br/>but it was the goddess athena that<br/>
breathed life into them.</h2>
  </Section>
)