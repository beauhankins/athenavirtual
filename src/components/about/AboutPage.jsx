import React from 'react'

import HeroSection from '../shared/HeroSection';
import QuoteSection from './QuoteSection';
import GiveLifeSection from './GiveLifeSection';
import GetTimeSection from './GetTimeSection';
import GuaranteeSection from '../shared/GuaranteeSection';
import EnquireSection from './EnquireSection';

export default () => (
  <div>
    <HeroSection
      backgroundImage={`${AssetsPath}/hero.jpg`}
    />
    <QuoteSection />
    <GiveLifeSection />
    <GetTimeSection />
    <GuaranteeSection
      backgroundColor="#CED9DE"
    />
    <EnquireSection />
  </div>
)