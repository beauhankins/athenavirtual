import React from 'react'
import Section from '../shared/Section';

export default () => (
  <Section
    className="getTimeSection"
    backgroundColor="#F5F5F4">
    <div className="contentWrapper">
      <h2 className="bold">#HardWorkIsForLosers</h2>
      <p>Athyna exists to change the way we work in the new economy. We believe that hiring a VA is possibly the best business decision you could make as you endeavour to scale your business. It’s affordable and it frees up your time to focus on the important decisions, the ones that are going to help your business grow.</p>
    </div>
    <div className="imageWrapper">
      <img src={`${AssetsPath}/getbacktime.png`} />
    </div>
  </Section>
)