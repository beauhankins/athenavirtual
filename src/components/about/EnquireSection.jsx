import React from 'react'
import Section from '../shared/Section';

export default () => (
  <Section
    className="enquireSection"
    backgroundColor="#F5F5F4">
    <h2 className="bold">interested in joining #TheAntiHustle?</h2>
    <a href="/enquire" className="button inline large">Enquire Now</a>
  </Section>
)