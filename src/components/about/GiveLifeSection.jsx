import React from 'react'
import Section from '../shared/Section';

export default () => (
  <Section
    className="giveLifeSection"
    backgroundColor="#F5F5F4">
    <div className="imageWrapper">
      <img src={`${AssetsPath}/givebacklife.png`} />
    </div>
    <div className="contentWrapper">
      <h2 className="bold">#GiveBackLife</h2>
      <p>The state of the economy in Venezuela is leaving educated and talented people out of work. Providing a steady wage and safe work conditions we allow Venezuelans to build a sustainable future and remain in their country to support their family and friends.</p>
    </div>
  </Section>
)