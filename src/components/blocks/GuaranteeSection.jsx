import React from 'react';
import Section from '../shared/Section';
import { linebreaks } from '../../lib/StringUtils';

export default ({ heading, content, textColor, backgroundColor }) => (
  <Section
    color={textColor}
    backgroundColor={backgroundColor}>
    <div className="guaranteeSection">
      <div className="guaranteeCard">
        <h5>Guarantee Section</h5>
        <h3>{heading}</h3>
        <p>{linebreaks(content)}</p>
      </div>
    </div>
  </Section>
)