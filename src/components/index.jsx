import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Header from './layout/Header';
import Footer from './layout/Footer';
import NotFound from './layout/NotFound';
import HomePage from './home/HomePage';
import BlogPage from './blog/BlogPage';
import BlogPostPage from './blog/BlogPostPage';
import PodcastPage from './podcast/PodcastPage';
import PodcastPostPage from './podcast/PodcastPostPage';
import AboutPage from './about/AboutPage';
import ProcessPage from './process/ProcessPage';
import EnquirePage from './enquire/EnquirePage';
import ThankYouPage from './thankyou/ThankYouPage';
import NewsletterCard from './shared/NewsletterCard';
import ScrollToTop from './shared/ScrollToTop';
import Api from '../lib/api';

// Load the Sass file
require('../style.scss');

const App = () => {
  return (
    <div>
      <Header />
      <div id="content">
        <ScrollToTop>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/blog" component={BlogPage} />
            <Route exact path="/blog/:postId" component={BlogPostPage} />
            <Route exact path="/podcast" component={PodcastPage} />
            <Route exact path="/podcast/:postId" component={PodcastPostPage} />
            <Route exact path="/about" component={AboutPage} />
            <Route exact path="/process" component={ProcessPage} />
            <Route exact path="/enquire" component={EnquirePage} />
            <Route exact path="/thankyou" component={ThankYouPage} />
            <Route path="*" component={NotFound} />
          </Switch>
        </ScrollToTop>
        <NewsletterCard />
      </div>
      <Footer />
    </div>
  );
};

async function init() {
  const props = {};

  window.AssetsPath = `${AthenaVirtual.URL.root}wp-content/themes/athenavirtual/dist/images/`;

  render((
    <Router>
      <Route path="/" render={() => <App {...props} />} />
    </Router>
  ), document.getElementById('main'));
}

init();