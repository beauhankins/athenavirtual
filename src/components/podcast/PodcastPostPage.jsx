import React from 'react';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { ArrowLeft } from 'react-feather';
import { PulseLoader as Loader } from 'react-spinners';
import Section from '../shared/Section';
import HeroSection from '../shared/HeroSection';
import Safe from 'react-safe';
import Api from '../../lib/api';

export default class PodcastPostPage extends React.Component {
  constructor() {
    super(...arguments);

    this.state = {
      post: null
    };

    this.api = new Api(AthenaVirtual.URL.api);
  }

  componentDidMount() {
    this.fetchPost();
  }

  fetchPost() {
    const { postId } = this.props.match.params;
    this.api.getAllPosts().then(posts =>
      this.setState({
        post: posts.find(post => post.slug === postId)
      })
    );
  }

  render() {
    const { post } = this.state;

    return (
      <div>
        {post && post.acf && post.acf.image &&
          <HeroSection
            style={{ backgroundPosition: 'cover' }}
            backgroundImage={post.acf.image}
          />
        }
        <Section>
          <Link to="/blog" className="inline"><div className="flex items-center"><ArrowLeft size={16} /> &nbsp;Back</div></Link>
          <br/>
          <br/>
          {!post && 
            <div className="flex justify-center"><Loader size={10} color="#3A455C"/></div>
          }
          {post &&
            <div className="podcastPost">
              {post.category && <h5>{post.category.name}</h5>}
              <h2>{post.title.rendered}</h2>
              <h5>{moment(post.date).format("MMMM Do YYYY")}</h5>
              {post.acf && post.acf.author && <h4>By {post.acf.author}</h4>}
              <br/>
              <Safe.p className="podcastPostContent">
                {post.content.rendered}
              </Safe.p>
            </div>
          }
        </Section>
      </div>
    );
  }
}