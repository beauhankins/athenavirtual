import React from 'react'
import Section from '../shared/Section';

export default class EnquirePage extends React.Component {
  render() {
    return (
      <Section backgroundColor="#DAE3E7" className="enquireFormSection align-center">
        <h3>SIGN UP WITH US AND GET 20 HOURS FREE!</h3>
        <p style={{ maxWidth: '40em', margin: '0 auto' }}>
          Getting set up with a Virtual Assistant is made easy with Athyna.<br/>
          Leave your details below and we'll give you a call to talk about your business's needs. Sign up with us TODAY and get 20 HOURS FREE!<br/>
          <br/>
          That's enough banter from us. Talk soon!
        </p>

        <form
          method="POST"
          action="https://athenavirtual.activehosted.com/proc.php"
          className="enquireForm"
          id="_form_1_">
          <input type="hidden" name="u" value="1" />
          <input type="hidden" name="f" value="1" />
          <input type="hidden" name="s" />
          <input type="hidden" name="c" value="0" />
          <input type="hidden" name="m" value="0" />
          <input type="hidden" name="act" value="sub" />
          <input type="hidden" name="v" value="2" />
          <div className="row">
            <input
              type="text"
              className="field"
              name="firstname"
              placeholder="First Name*"
              required
            />
            <input
              type="text"
              className="field"
              name="lastname"
              placeholder="Last Name*"
              required
            />
          </div>
          <div className="row">
            <input
              type="text"
              className="field"
              name="email"
              placeholder="Email*"
              required
            />
          </div>
          <div className="row">
            <input
              type="text"
              className="field"
              name="phone"
              placeholder="Phone*"
              required
            />
          </div>
          <div className="row">
            <select className="field" name="field[2]" required>
              <option selected>
                Preferred Package...
              </option>
              <option value="Entry">
                Entry
              </option>
              <option value="Basic">
                Basic
              </option>
              <option value="Premium">
                Premium
              </option>
            </select>
            <input
              type="text"
              className="field"
              name="field[3]"
              placeholder="Industry Type*"
              required
            />
            <input
              type="text"
              className="field"
              name="field[4]"
              placeholder="Promo Code"
            />
          </div>
          <button id="_form_1_submit" type="submit" className="button">
            Submit
          </button>
        </form>
      </Section>
    );
  }
}