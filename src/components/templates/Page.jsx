import React from 'react';
import camelcaseKeys from 'camelcase-keys';
import HeroSection from '../blocks/HeroSection';
import FourStepSection from '../blocks/FourStepSection';
import VideoSection from '../blocks/VideoSection';
import GuaranteeSection from '../blocks/GuaranteeSection';

const blockTypes = {
  'block-lab/hero-section': HeroSection,
  'block-lab/four-step-section': FourStepSection,
  'block-lab/video-section': VideoSection,
  'block-lab/guarantee-section': GuaranteeSection,
};

export default ({ page }) => {
  const blocks = page.blocks.filter(block => !!block.blockName);

  const renderBlock = block => {
    const BlockComponent = blockTypes[block.blockName];
    if (!BlockComponent) {
      return <div dangerouslySetInnerHTML={{ __html: block.innerHTML }} />;
    }
    return <BlockComponent {...camelcaseKeys(block.attrs)} />
  };

  return (
    <div>
      {blocks.map(block => renderBlock(block))}
    </div>
  );
};