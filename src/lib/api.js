export default class Api {
  constructor(url) {
    this.url = url;
  }

  getAllPosts() {
    return new Promise((resolve, reject) => {
      const endpoint = `${this.url}/posts`;
      fetch(endpoint)
        .then(response => response.json())
        .then(posts => this.mapCategories(posts))
        .then(posts => resolve(posts))
        .catch(error => console.error(error));
    })
  }

  getPost(postId) {
    return new Promise((resolve, reject) => {
      const endpoint = `${this.url}/posts/${postId}`;
      fetch(endpoint)
        .then(response => response.json())
        .then(post => this.mapCategories([post]))
        .then(posts => resolve(post[0]))
        .catch(error => console.error(error));
    })
  }

  getAllPages() {
    return new Promise((resolve, reject) => {
      const endpoint = `${this.url}/pages`;
      fetch(endpoint)
        .then(response => response.json())
        .then(pages => resolve(pages))
        .catch(error => console.error(error));
    })
  }

  getPage(pageId) {
    return new Promise((resolve, reject) => {
      const endpoint = `${this.url}/pages/${pageId}`
      fetch(endpoint)
        .then(response => response.json())
        .then(page => resolve(page))
        .catch(error => console.error(error))
    })
  }

  getUser(userId) {
    return new Promise((resolve, reject) => {
      const endpoint = `${this.url}/users/${userId}`
      fetch(endpoint)
        .then(response => response.json())
        .then(user => resolve(user))
        .catch(error => console.error(error))
    })
  }

  getAllCategories() {
    return new Promise((resolve, reject) => {
      const endpoint = `${this.url}/categories`
      fetch(endpoint)
        .then(response => response.json())
        .then(categories => resolve(categories))
        .catch(error => console.error(error))
    })
  }

  mapCategories(posts) {
    return new Promise((resolve, reject) => {
      this.getAllCategories()
        .then(categories => resolve(
          posts.map(post => this.mapCategory(post, categories))
        ))
    })
  }

  mapCategory(post, categories) {
    const categoryId = post.categories[0];
    if (!categoryId || categoryId === 1) { return post }
    return Object.assign(post, {
      category: categories.find(({ id }) => id === categoryId)
    });
  }
}