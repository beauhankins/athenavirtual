<?php
function athena_scripts() {

  // Load stylesheets
  wp_enqueue_style('athena-style-dist', get_stylesheet_directory_uri() . '/dist/style.css');
  wp_enqueue_style('athena-style', get_stylesheet_uri());

  // Load scripts
  wp_enqueue_script('athena-script', get_stylesheet_directory_uri() . '/dist/app.js' , array(), '1.0', true);

  $url = trailingslashit(home_url());
  $path = trailingslashit(parse_url($url, PHP_URL_PATH));

  wp_scripts()->add_data('athena-script', 'data', sprintf('var AthenaVirtual = %s;', wp_json_encode(array(
    'title' => get_bloginfo('name', 'display'),
    'path' => $path,
    'URL' => array(
      'api' => esc_url_raw(get_rest_url(null, '/wp/v2')),
      'root' => esc_url_raw($url),
      'template' => get_template_directory_uri(),
    ),
  ))));
}
add_action('wp_enqueue_scripts', 'athena_scripts');

add_action('rest_api_init', function () {

		if (!function_exists('use_block_editor_for_post_type')) {
			require ABSPATH . 'wp-admin/includes/post.php';
		}

		// Surface all Gutenberg blocks in the WordPress REST API
		$post_types = get_post_types_by_support(['editor']);
		foreach ($post_types as $post_type) {
			if (use_block_editor_for_post_type($post_type)) {
				register_rest_field($post_type, 'blocks', [
          'get_callback' => function (array $post) {
            return parse_blocks($post['content']['raw']);
          },
				]);
			}
		}
	}
);